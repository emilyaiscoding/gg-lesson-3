import { Switch, BrowserRouter as Router, Route } from "react-router-dom";
import React from 'react';
import './styles/styles.scss';
import Dashboard from "./app/modules/dashboard/ui/pages/index";
import Contacts from "./app/modules/contacts/ui/pages/index";

const App = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={Dashboard} />
                <Route exact path="/contacts" component={Contacts} />
            </Switch>
        </Router>
    );
}

export default App;
