import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import "./styles/libs/bootstrap.scss";

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);
