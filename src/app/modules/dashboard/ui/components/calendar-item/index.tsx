import React from 'react';
import './styles.scss';

export type CalendarItem = {
    date: Date
};

type Props = {
    calendarItem: CalendarItem
};

const CalendarItem = (props: Props) => {
    return (
        <div className="calendar-item d-flex flex-column">
            <div className="d-flex justify-content-center day-text">
                {/* {props.calendarItem.date.getDay()} */}
                Sat
            </div>
            <div className="d-flex justify-content-center date-text">
                {props.calendarItem.date.getDate()}
            </div>
        </div>
    );
};

export default CalendarItem;
