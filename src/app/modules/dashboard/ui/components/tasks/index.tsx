import React from 'react';
import ProgressBar from '../progress-bar';
import Calendar from '../calendar';
import TasksList from '../tasks-list';
import './styles.scss';

const Tasks = () => {
    return (
        <div className="tasks">
            <ProgressBar progress={{ "done": 10, "total": 10 }} />
            <Calendar calendarItems={calendarItems} />
            <TasksList taskItems={taskItems} />
        </div>
    );
};

const taskItems = [
    {
        "title": "Send benefit review by Sunday",
        "type": "Reminder",
        "dueDate": new Date(),
        "assignee": { "name": "George Fields", "icon": "/associated_photo.png" },
        "status": "Completed"
    },
    {
        "title": "Invite to office meet-up",
        "type": "Call",
        "dueDate": new Date(),
        "assignee": { "name": "Rebecca Moore", "icon": "/associated_photo.png" },
        "status": "Ended"
    },
    {
        "title": "Office meet-up",
        "type": "Event",
        "dueDate": new Date(),
        "assignee": { "name": "Lindsey Stroud", "icon": "associated_photo.png" },
        "status": "Completed"
    }
];

const calendarItems = [
    { "date": new Date() },
    { "date": new Date() },
    { "date": new Date() },
    { "date": new Date() },
    { "date": new Date() },
    { "date": new Date() },
    { "date": new Date() }
];

export default Tasks;
