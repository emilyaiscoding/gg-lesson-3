import React from 'react';
import TaskItemComponent, { TaskItem } from '../task-item';
import { Link } from '../../../../ud-ui/link/styles';

type Props = {
    taskItems: TaskItem[]
};

const TasksList = (props: Props) => {
    return (
        <div>
            {props.taskItems.map(item => (
                <TaskItemComponent key={Math.random()} taskItem={item} />
            ))}
            <div className="d-flex justify-content-center">
                <Link>Show more</Link>
            </div>
        </div>
    );
};

export default TasksList;
