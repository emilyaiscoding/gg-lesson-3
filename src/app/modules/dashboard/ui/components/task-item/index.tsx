import React from 'react';
import './styles.scss';
import Badge from '../../../../../modules/ud-ui/badge';
import AssigneeComponent, { Assignee } from '../assignee';

export type TaskItem = {
    title: String,
    type: String,
    dueDate: Date,
    assignee: Assignee,
    status: String
};

type Props = {
    taskItem: TaskItem
};

const color = (status: String) => status === "Completed" ? "#2ED47A" : "#F7685B";

const TaskItem = (props: Props) => {
    return (
        <div className="task-item mx-3 mt-3 p-3">
            <div className="d-flex justify-content-between">
                <div className="task-title">{props.taskItem.title}</div>
                <div className="task-type">{props.taskItem.type}</div>
            </div>
            <div className="due-date">
                <span className="due-date-title">Due date: </span>
                {props.taskItem.dueDate.toLocaleDateString(
                    "en-US",
                    { year: "numeric", month: "long", day: "numeric" }
                )}
            </div>
            <div className="mt-4 d-flex justify-content-between">
                <AssigneeComponent assignee={props.taskItem.assignee} />
                <Badge label={props.taskItem.status} color={color(props.taskItem.status)} />
            </div>
        </div>
    );
};

export default TaskItem;
