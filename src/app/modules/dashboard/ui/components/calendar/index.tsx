import React from 'react';
import CalendarItemComponent, { CalendarItem } from '../calendar-item';
import './styles.scss';

type Props = {
    calendarItems: CalendarItem[]
};

const Calendar = (props: Props) => {
    return (
        <div className="">
            <div className="calendar-title px-3">
                23 December, Sunday
            </div>
            <div className="calendar-items py-4 px-4 d-flex justify-content-between">
                {props.calendarItems.map(item => (
                    <CalendarItemComponent key={Math.random()} calendarItem={item} />
                ))}
            </div>
        </div>
    );
};

export default Calendar;
