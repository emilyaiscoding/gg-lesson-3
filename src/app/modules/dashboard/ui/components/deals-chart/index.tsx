import React from 'react';
import Select from '../../../../ud-ui/select';
import './styles.scss';
import { CSSProperties } from 'styled-components';

type Props = {
    plot: String
};

const image = (props: Props): CSSProperties => {
    return {
        "backgroundImage": `url(${props.plot}`
    } as React.CSSProperties;
};

const DealsChart = (props: Props) => {
    return (
        <div className="deals mb-4">
            <div className="deals-header p-3 d-flex justify-content-between">
                <div className="deals-title">Deals</div>
                <Select select={
                    {
                        "label": "Show",
                        "options": [{ "label": "Monthly", "value": "" }]
                    }
                } />
            </div>
            <div className="p-3">
                <div className="deals-chart" style={image(props)}></div>
            </div>
        </div>
    );
};

export default DealsChart;
