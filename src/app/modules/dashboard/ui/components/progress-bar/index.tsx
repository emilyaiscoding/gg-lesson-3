import React from 'react';
import './styles.scss';
import Select from '../../../../ud-ui/select';

export type Progress = {
    done: Number,
    total: Number
};

type Props = {
    progress: Progress
};

const ProgressBar = (props: Props) => {
    return (
        <div className="p-3">
            <div className="d-flex justify-content-between">
                <div className="progress-bar-text">
                    {props.progress.done} tasks completed out of {props.progress.total}
                </div>
                <Select select={
                    {
                        "label": "Show",
                        "options": [{ "value": "", "label": "This week" }]
                    }
                } />
            </div>
            <div className="progress-bar my-4">
            </div>
        </div>
    );
};

export default ProgressBar;
