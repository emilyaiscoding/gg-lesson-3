import React from 'react';
import Select from '../../../../ud-ui/select';
import './styles.scss';
import { CSSProperties } from 'styled-components';

type Props = {
    plot: String
};

const image = (props: Props): CSSProperties => {
    return {
        "backgroundImage": `url(${props.plot}`
    } as React.CSSProperties;
};

const TasksChart = (props: Props) => {
    return (
        <div className="tasks-chart-box">
            <div className="tasks-chart-header p-3 d-flex justify-content-between">
                <div className="tasks-chart-title">Tasks</div>
                <Select select={
                    {
                        "label": "Show",
                        "options": [{ "label": "This month", "value": "" }]
                    }
                } />
            </div>
            <div className="p-3">
                <div className="tasks-chart" style={image(props)}></div>
            </div>
        </div>
    );
};

export default TasksChart;
