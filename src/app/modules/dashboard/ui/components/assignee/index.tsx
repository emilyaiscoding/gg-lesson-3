import React from 'react';
import { CSSProperties } from 'styled-components';
import './styles.scss';

export type Assignee = {
    name: String,
    icon: String
};

type Props = {
    assignee: Assignee
};

const image = (props: Props): CSSProperties => {
    return {
        "backgroundImage": `url(${props.assignee.icon})`
    } as React.CSSProperties;
};

const Assignee = (props: Props) => {
    return (
        <div className="d-flex justify-content-between align-items-center">
            <div className="task-icon" style={image(props)}></div>
            <div className="task-assignee">{props.assignee.name}</div>
        </div>
    );
};

export default Assignee;
