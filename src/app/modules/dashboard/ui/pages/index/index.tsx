import React from 'react';
import Container from '../../../../grid/ui/Container';
import Row from '../../../../grid/ui/Row';
import SideBar from '../../../../layout/ui/components/side-bar/SideBar';
import Col from '../../../../grid/ui/Col';
import SearchBar from '../../../../layout/ui/components/search-bar/SearchBar';
import Tasks from '../../components/tasks';
import DealsChart from '../../components/deals-chart';
import TasksChart from '../../components/tasks-chart';

const Dashboard = () => {
    return (
        <Container breakpoint="fluid">
            <Row>
                <div className="col-2 with-shadow">
                    <SideBar sideBarItems={items} />
                </div>
                <Col width="10">
                    <Row>
                        <SearchBar placeholder="Global search" />
                    </Row>
                    <div className="row p-5">
                        <div className="col-7 p-0">
                            <Tasks></Tasks>
                        </div>
                        <div className="col-5 pl-5">
                            <DealsChart plot="/deals_graphic.png" />
                            <TasksChart plot="/deals_graphic.png" />
                        </div>
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

const items = [
    {
        "icon": "/dashboard.png",
        "title": "Dashboard"
    },
    {
        "icon": "/tasks.png",
        "title": "Tasks"
    },
    {
        "icon": "/email.png",
        "title": "Email"
    },
    {
        "icon": "/contacts.png",
        "title": "Contacts"
    },
    {
        "icon": "/chat.png",
        "title": "Chat"
    },
    {
        "icon": "/deals.png",
        "title": "Deals"
    }
];

export default Dashboard;
