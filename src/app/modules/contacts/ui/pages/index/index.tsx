import React from 'react';
import Container from '../../../../grid/ui/Container';
import Row from '../../../../grid/ui/Row';
import SideBar from '../../../../layout/ui/components/side-bar/SideBar';
import SearchBar from '../../../../layout/ui/components/search-bar/SearchBar';
import Col from '../../../../grid/ui/Col';
import Select from '../../../../ud-ui/select';
import { Button } from '../../../../ud-ui/button/styles';
import ContactsList from '../../components/contacts-list';

const Contacts = () => {
    return (
        <Container breakpoint="fluid">
            <Row>
                <div className="col-2 with-shadow">
                    <SideBar sideBarItems={items} />
                </div>
                <Col width="10">
                    <Row>
                        <SearchBar placeholder="Search for a contact" />
                    </Row>
                    <div className="row px-4">
                        <Col>
                            <div className="row d-flex align-items-end justify-content-between px-3 pl-4 pt-5 pb-3">
                                <Select select={
                                    {
                                        "label": "Company",
                                        "options": [{ "label": "All", "value": "" }]
                                    }
                                } />
                                <Button>Add contact</Button>
                            </div>
                            <Row>
                                <Col>
                                    <ContactsList contacts={contacts} />
                                </Col>
                            </Row>
                        </Col>
                    </div>
                </Col>
            </Row>
        </Container>
    );
};

const contacts = new Array(10).fill({
    "icon": "/contact_photo.png",
    "name": "Lindsey Stroud",
    "email": "lindsey.stroud@gmail.com",
    "company": "Hatchbuck",
    "role": "Manager",
    "forecast": "50 %",
    "recentActivity": "5 Minutes ago"
});

const items = [
    {
        "icon": "/dashboard.png",
        "title": "Dashboard"
    },
    {
        "icon": "/tasks.png",
        "title": "Tasks"
    },
    {
        "icon": "/email.png",
        "title": "Email"
    },
    {
        "icon": "/contacts.png",
        "title": "Contacts"
    },
    {
        "icon": "/chat.png",
        "title": "Chat"
    },
    {
        "icon": "/deals.png",
        "title": "Deals"
    }
];

export default Contacts;
