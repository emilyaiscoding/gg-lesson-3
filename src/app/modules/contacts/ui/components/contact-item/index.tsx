import React from 'react';

export type Contact = {
    icon: string,
    name: string,
    email: string,
    company: string,
    role: string,
    forecast: string,
    recentActivity: string
};

type Props = {
    children: any,
    contact: Contact
};

const ContactItem = (props: Props) => {
    return null;
};

export default ContactItem;
