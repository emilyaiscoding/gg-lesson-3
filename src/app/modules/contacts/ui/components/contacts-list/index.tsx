import React, { CSSProperties } from 'react';
import { Contact } from '../contact-item';
import Table from '../../../../ud-ui/table';
import "./styles.scss";

type Props = {
    contacts: Contact[]
};

const image = (url: String): CSSProperties => {
    return {
        "backgroundImage": `url(${url})`
    } as React.CSSProperties;
};

const addAvatar = (contact: Contact): Contact => {
    let newContact: any = { ...contact };
    newContact.name = (<div className="d-flex align-items-center">
        <div className="avatar" style={image(contact.icon)}></div>
        {contact.name}
    </div>);
    return newContact;
};

const ContactsList = (props: Props) => {
    return (
        <Table header={header} items={props.contacts.map(addAvatar)} />
    );
};

const header = [
    { key: "name", label: "Name" },
    { key: "email", label: "Email" },
    { key: "company", label: "Company name" },
    { key: "role", label: "Role" },
    { key: "forecast", label: "Forecast" },
    { key: "recentActivity", label: "Recent activity" }
];

export default ContactsList;
