import styled from 'styled-components';

export const Link = styled.button`
    color: #109CF1;
    font-weight: 500;
    font-family: Poppins;
    font-size: 13px;
    padding: 1em;
    border: none;
    background-color: transparent;
`
