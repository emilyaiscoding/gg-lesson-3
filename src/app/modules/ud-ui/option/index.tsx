import React from 'react';

export type Option = {
    value: String,
    label: String
};

type Props = {
    option: Option
};

const Option = (props: Props) => {
    return null;
};

export default Option;
