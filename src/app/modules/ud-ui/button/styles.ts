import styled from 'styled-components';

export const Button = styled.button`
    background-color: #109CF1;
    color: white;
    font-family: Poppins;
    font-size: 13px;
    font-weight: 500;
    height: 3em;
    width: 11em;
    text-align: center;
    vertical-align: middle;
    line-height: 2em;
    border-radius: 4px;
    border: none;
`
