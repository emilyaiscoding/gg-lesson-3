import React from 'react';
import './styles.scss';

type Props = {
    values: String[]
};

const TableRow = (props: Props) => {
    return (
        <tr className="border-bottom">
            <td>
                <input type="checkbox" />
            </td>
            {props.values.map(item => (
                <td key={Math.random()}>{item}</td>
            ))}
        </tr>
    );
};

export default TableRow;
