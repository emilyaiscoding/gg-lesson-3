import React from 'react';
import TableRow from './table-row';
import './styles.scss';

type Props = {
    items: Item[],
    header: Item[]
};

export type Item = {
    [key: string]: string
};

const objectToRow = (header: Item[], object: Item): any[] => {
    return header.map(entry => object[entry.key]);
};

const Table = (props: Props) => {
    return (
        <table>
            <thead>
                <tr>
                    <td>
                        <input type="checkbox" />
                    </td>
                    {props.header.map(item => (
                        <th key={Math.random()}>{item.label}</th>
                    ))}
                </tr>
            </thead>
            <tbody>
                {props.items.map(item => (
                    <TableRow key={Math.random()} values={objectToRow(props.header, item)} />
                ))}
            </tbody>
        </table>
    );
};

export default Table;
