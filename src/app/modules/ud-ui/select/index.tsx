import React from 'react';
import { Option } from '../option';
import { StyledSelect } from './styles';

export type Select = {
    label: String,
    options: Option[]
};

type Props = {
    select: Select
};

const Select = (props: Props) => {
    return (
        <div>
            <StyledSelect>
                {props.select.label}: {props.select.options[0].label}
            </StyledSelect>
        </div>
    );
};

export default Select;
