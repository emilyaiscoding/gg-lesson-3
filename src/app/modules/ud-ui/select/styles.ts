import styled from 'styled-components';

export const StyledSelect = styled.div`
    font-weight: 400;
    font-family: 'Poppins', sans-serif;
    font-size: 12px;
    color: #6A707E;
`
