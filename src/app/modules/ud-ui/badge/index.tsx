import React from 'react';
import { ColoredBadge } from './styles';

type Props = {
    label: String,
    color: string
};

const Badge = (props: Props) => {
    return (
        <ColoredBadge color={props.color}>
            {props.label}
        </ColoredBadge>
    );
};

export default Badge;
