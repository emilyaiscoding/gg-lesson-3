import styled from 'styled-components';

export const ColoredBadge = styled.div`
    background-color: ${props => props.color};
    color: white;
    font-family: Poppins;
    font-size: 11px;
    font-weight: 500;
    height: 2em;
    width: 8em;
    text-align: center;
    vertical-align: middle;
    line-height: 2em;
    border-radius: 4px;
`
