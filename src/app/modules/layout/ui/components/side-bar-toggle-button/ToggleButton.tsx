import React from 'react';
import './styles.scss';

const ToggleButton = () => {
    return (
        <div className="toggle-button row">
            <div className="toggle-icon mx-2"></div>
            Toggle sidebar
        </div>
    );
};

export default ToggleButton;
