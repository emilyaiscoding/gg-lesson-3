import React from 'react';
import './styles.scss';
import { NotificationsButton } from '../notification-button/styles';

type Props = {
    placeholder: String
};

const SearchBar = (props: Props) => {
    return (
        <div className="search-bar d-flex justify-content-between align-items-center">
            <div className="d-flex align-items-center">
                <div className="search-icon mx-3"></div>
                <input type="text" placeholder={props.placeholder.toString()} className="search-input" />
            </div>
            <NotificationsButton />
        </div>
    );
};

export default SearchBar;
