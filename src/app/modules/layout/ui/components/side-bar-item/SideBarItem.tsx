import React from 'react';
import "./styles.scss";
import { CSSProperties } from 'styled-components';

export type SideBarItem = {
    title: String,
    icon: String
};

type Props = {
    sideBarItem: SideBarItem
};

const image = (props: Props): CSSProperties => {
    return {
        "backgroundImage": `url(${props.sideBarItem.icon}`
    } as React.CSSProperties;
};

const SideBarItem = (props: Props) => {
    return (
        <div className="side-bar-item ml-4 my-3 d-flex">
            <div className="side-bar-icon mr-2" style={image(props)}></div>
            <div className="side-bar-item-text">
                {props.sideBarItem.title}
            </div>
        </div>
    );
};

export default SideBarItem;
