import React from 'react';
import SideBarItemComponent, { SideBarItem } from '../side-bar-item/SideBarItem';
import './styles.scss';

type Props = {
    sideBarItems: SideBarItem[]
};

const SideBarItems = (props: Props) => {
    return (
        <div className="side-bar-items">
            {props.sideBarItems.map(item => (<SideBarItemComponent key={Math.random()} sideBarItem={item} />))}
        </div>
    );
};

export default SideBarItems;
