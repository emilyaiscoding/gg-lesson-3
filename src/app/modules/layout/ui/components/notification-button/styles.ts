import styled from 'styled-components';

export const NotificationsButton = styled.button`
    width: 1.5rem;
    padding: 0;
    height: 1.5rem;
    border: none;
    margin-right: 5rem;
    background-color: transparent;
    background-repeat: no-repeat;
    background-image: url("/notifications.png");
`
