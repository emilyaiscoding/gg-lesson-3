import React from 'react';
import SideBarItemComponent, { SideBarItem } from '../side-bar-item/SideBarItem';
import './styles.scss';
import Logo from '../logo/Logo';
import UserInfo from '../user-info/UserInfo';
import SideBarItems from '../side-bar-items/SideBarItems';
import ToggleButton from '../side-bar-toggle-button/ToggleButton';

type Props = {
    sideBarItems: SideBarItem[]
};

const SideBar = (props: Props) => {
    return (
        <div className="side-bar">
            <Logo />
            <UserInfo userInfo={
                {
                    "name": "Sierra Ferguson",
                    "email": "s.ferguson@gmail.com",
                    "icon": "icon"
                }
            } />
            <SideBarItems sideBarItems={props.sideBarItems} />
            <SideBarItemComponent sideBarItem={
                {
                    "icon": "/settings.png",
                    "title": "Settings"
                }
            } />
            <ToggleButton />
        </div>
    );
};

export default SideBar;
