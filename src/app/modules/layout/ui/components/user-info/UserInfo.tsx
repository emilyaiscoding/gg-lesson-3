import React from 'react';
import './styles.scss';

export type UserInfo = {
    name: String,
    email: String,
    icon: String
};

type Props = {
    userInfo: UserInfo
};

const UserInfo = (props: Props) => {
    return (
        <div className="user-info m-4 row">
            <div className="profile-avatar"></div>
            <div className="ml-3">
                <div className="profile-name">
                    {props.userInfo.name}
                </div>
                <div className="profile-email">
                    {props.userInfo.email}
                </div>
            </div>
        </div>

    );
};

export default UserInfo;
