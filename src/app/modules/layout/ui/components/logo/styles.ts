import styled from 'styled-components';

export const StyledLogo = styled.div`
    color: #109cf1;
    font-size: 18px;
    font-family: 'Open Sans', sans-serif;
    border-bottom: 1px solid #EBEFF2;
    padding: 1.5rem;
`
