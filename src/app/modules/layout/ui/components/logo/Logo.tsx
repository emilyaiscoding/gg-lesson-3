import React from 'react';
import { StyledLogo } from "./styles";

const Logo = () => {
    return (
        <StyledLogo>SaaS Kit</StyledLogo>
    );
};

export default Logo;
