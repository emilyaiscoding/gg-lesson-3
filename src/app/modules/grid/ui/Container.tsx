import React from 'react';
import 'bootstrap/dist/css/bootstrap-grid.css';

const Container = (props: any) => (
    <div className={"container" + (props.breakpoint ? `-${props.breakpoint}` : "")}>
        {props.children}
    </div>
);

export default Container;
