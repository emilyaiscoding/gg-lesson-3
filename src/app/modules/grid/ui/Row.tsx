import React from 'react';
import 'bootstrap/dist/css/bootstrap-grid.css';

const alignItemsClass = (props: { alignItems: String }) =>
    props.alignItems ? `align-items-${props.alignItems}` : "";

const justifyContentClass = (props: { justifyContent: String }) =>
    props.justifyContent ? `justify-content-${props.justifyContent}` : "";

const Row = (props: any) => (
    <div className={`row ${alignItemsClass(props)} ${justifyContentClass(props)}`}>
        {props.children}
    </div>
);

export default Row;
