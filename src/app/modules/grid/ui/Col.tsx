import React from 'react';
import 'bootstrap/dist/css/bootstrap-grid.css';

const allowedModifier = (modifier: string) =>
    ["xs", "sm", "md", "lg", "xl"].indexOf(modifier) !== -1;

const colModifiers = (options: any) => [
    "col" + (options.width ? `-${options.width}` : ""),
    ...Object.keys(options).filter(allowedModifier)
        .map(key => ["col", key, options[key]].join("-"))
].join(" ");

const Col = (props: any) => (
    <div className={colModifiers(props)}>
        {props.children}
    </div>
);

export default Col;
